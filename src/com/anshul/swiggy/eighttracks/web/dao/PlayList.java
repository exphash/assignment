package com.anshul.swiggy.eighttracks.web.dao;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PlayList {

	private int id;
	private String name;
	private int playcount;
	
	@JsonIgnore
	private int userid;
	private int favourite;
	
	@JsonIgnore
	private int weightage;
	private List<Tag> taglist;
	
	
	public PlayList(){
	
	}
	
	public PlayList(int id, String name, int playcount, int userid) {
		super();
		this.id = id;
		this.name = name;
		this.playcount = playcount;
		this.userid = userid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPlaycount() {
		return playcount;
	}

	public void setPlaycount(int playcount) {
		this.playcount = playcount;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getFavourite() {
		return favourite;
	}

	public void setFavourite(int favourite) {
		this.favourite = favourite;
	}

	public int getWeightage() {
		return weightage;
	}

	public void setWeightage(int weightage) {
		this.weightage = weightage;
	}
	
	public List<Tag> getTaglist() {
		return taglist;
	}

	public void setTaglist(List<Tag> taglist) {
		this.taglist = taglist;
	}

	@Override
	public String toString() {
		return "PlayList [id=" + id + ", name=" + name + ", playcount=" + playcount + ", userid=" + userid + "]";
	}
	
}
