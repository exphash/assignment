package com.anshul.swiggy.eighttracks.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

@Component("tagDAO")
public class TagDAO {

	private NamedParameterJdbcTemplate jdbc;
	
	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Tag> getTag(String id) {

		String sql = "select * from tag where id = "+id;
		
		return jdbc.query(sql, new RowMapper<Tag>() {

			public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
				Tag tag = new Tag();
				tag.setId(rs.getInt("id"));
				tag.setName(rs.getString("name"));

				return tag;
			}

		});

	}
	
	public Tag createTag(String name){
		
		String sql = "select * from tag order by id desc limit 1";
		
		Tag t =  jdbc.query(sql, new RowMapper<Tag>() {

			public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
				Tag tag = new Tag();
				tag.setId(rs.getInt("id"));
				tag.setName(rs.getString("name"));

				return tag;
			}
		}).get(0);

		t.setName(name);
		t.setId(t.getId()+1);
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(t);
		
		if(jdbc.update("insert into tag (id, name) values (:id, :name)", params) == 1){
			Tag tag =  jdbc.query(sql, new RowMapper<Tag>() {

				public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
					Tag tag = new Tag();
					tag.setId(rs.getInt("id"));
					tag.setName(rs.getString("name"));

					return tag;
				}
			}).get(0);
			
			return tag; 
		}
		
		return null;
	}
	
	public List<Tag> getPlayListCountByTag(String tagid) {
		
		
		String sql = "select count(tpm.playlist_id) as playlistcount, t.id as tagid, t.name tagname"+
				" from tag t , tag_playlist_map tpm where tpm.tagid = t.id and tpm.tagid = "+tagid+" group by t.id"; 

		return jdbc.query(sql, new RowMapper<Tag>() {
	
		public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
			Tag tag = new Tag();
			tag.setId(rs.getInt("tagid"));
			tag.setName(rs.getString("tagname"));
			tag.setPlaylistcount(rs.getInt("playlistcount"));
			
			return tag;
		}
	});

	}
	
	public List<Tag> getPlayListCountByTags() {
		String sql = "select count(tpm.playlist_id) as playlistcount, t.id as tagid, t.name tagname"+
						" from tag t left join  tag_playlist_map tpm on tpm.tagid = t.id group by t.id order by playlistcount desc";

		return jdbc.query(sql, new RowMapper<Tag>() {

			public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
				Tag tag = new Tag();
				tag.setId(rs.getInt("tagid"));
				tag.setName(rs.getString("tagname"));
				tag.setPlaylistcount(rs.getInt("playlistcount"));
				
				return tag;
			}
		});

	}
	
	public List<Tag> getAllTags() {

		String sql = "select * from tag";
		
		return jdbc.query(sql, new RowMapper<Tag>() {

			public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
				Tag tag = new Tag();
				tag.setId(rs.getInt("id"));
				tag.setName(rs.getString("name"));

				return tag;
			}
		});

	}
	
	public List<Tag> getTagsForPlaylist(String playlistid){
		
		String sql = "select t.id as id, t.name as name from tag t, tag_playlist_map tpm where t.id = tpm.tagid and tpm.playlist_id = "+playlistid;
		
		return jdbc.query(sql, new RowMapper<Tag>() {

			public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
				Tag tag = new Tag();
				tag.setId(rs.getInt("id"));
				tag.setName(rs.getString("name"));

				return tag;
			}
		});

	}


}

