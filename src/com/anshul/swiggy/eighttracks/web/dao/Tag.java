package com.anshul.swiggy.eighttracks.web.dao;

public class Tag {

	private int id;
	private String name;
	private int playlistcount = 0;
	
	public Tag(){
		
	}
	
	public Tag(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getPlaylistcount() {
		return playlistcount;
	}

	public void setPlaylistcount(int playlistcount) {
		this.playlistcount = playlistcount;
	}

	@Override
	public String toString() {
		return "Tags [id=" + id + ", name=" + name + "]";
	}
}
