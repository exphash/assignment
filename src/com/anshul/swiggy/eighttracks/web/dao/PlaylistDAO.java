package com.anshul.swiggy.eighttracks.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

@Component("playlistDAO")
public class PlaylistDAO {

	private NamedParameterJdbcTemplate jdbc;
	
	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public boolean addTagToPlaylist(String tagid, String playlistid){
		String sql = "insert into tag_playlist_map (tagid, playlist_id) values (:tagid, :playlistid)";
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("tagid", tagid);
		params.addValue("playlistid", playlistid);
		
		return jdbc.update(sql, params)==1;
	}
	
	public PlayList getPlaylist(String id) {

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);

		String sql = "select * from playlist where id = :id";
		
		return jdbc.queryForObject(sql, params, new RowMapper<PlayList>() {

			public PlayList mapRow(ResultSet rs, int rowNum) throws SQLException {
				PlayList playlist = new PlayList();
				
				playlist.setId(rs.getInt("id"));
				playlist.setName(rs.getString("name"));
				playlist.setPlaycount(rs.getInt("playcount"));				
				
				return playlist;
			}

		});

	}
	
	public List<PlayList> getAllPlaylists() {

		String sql = "select * from playlist";
		
		return jdbc.query(sql, new RowMapper<PlayList>() {

			public PlayList mapRow(ResultSet rs, int rowNum) throws SQLException {
				PlayList playlist = new PlayList();
				
				playlist.setId(rs.getInt("id"));
				playlist.setName(rs.getString("name"));
				playlist.setPlaycount(rs.getInt("playcount"));
				playlist.setUserid(rs.getInt("userid"));
				
				return playlist;
			}

		});

	}

	public List<PlayList> getPlaylistByCountAndFavs(){
		String sql = "select count(f.userid) as favourite, p.id id, p.name name, p.playcount as playcount, p.userid as userid " + 
							" from favourite f, playlist p where f.playlist_id = p.id" +
									" group by f.playlist_id";
		
		return jdbc.query(sql, new RowMapper<PlayList>() {

			public PlayList mapRow(ResultSet rs, int rowNum) throws SQLException {
				PlayList playlist = new PlayList();
				
				playlist.setId(rs.getInt("id"));
				playlist.setName(rs.getString("name"));
				playlist.setPlaycount(rs.getInt("playcount"));
				playlist.setFavourite(rs.getInt("favourite"));
				playlist.setUserid(rs.getInt("userid"));
				
				return playlist;
			}

		});

	}
	
	public List<PlayList> getPlayListsByTag(String tagid) {

		String sql = "select p.name as name, p.id as id,  p.playcount as playcount from tag_playlist_map tpm, playlist p where tpm.playlist_id = p.id"
				+ " and tpm.tagid = "+tagid;
		
		return jdbc.query(sql, new RowMapper<PlayList>() {

			public PlayList mapRow(ResultSet rs, int rowNum) throws SQLException {
				PlayList playlist = new PlayList();
				
				playlist.setId(rs.getInt("id"));
				playlist.setName(rs.getString("name"));
				playlist.setPlaycount(rs.getInt("playcount"));
				
				return playlist;
			}

		});
	}

}
