package com.anshul.swiggy.eighttracks.web.dao;

public class Song {

	private int id;
	private String name;
	private String metadata;
	
	public Song() {
		
	}
	
	public Song(int id, String name, String metadata) {
		super();
		this.id = id;
		this.name = name;
		this.metadata = metadata;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMetadata() {
		return metadata;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	@Override
	public String toString() {
		return "Song [id=" + id + ", name=" + name + ", metadata=" + metadata + "]";
	}
}
