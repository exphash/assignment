package com.anshul.swiggy.eighttracks.web.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.algolia.search.APIClient;
import com.algolia.search.ApacheAPIClientBuilder;
import com.algolia.search.Index;
import com.algolia.search.exceptions.AlgoliaException;
import com.algolia.search.objects.Query;
import com.algolia.search.objects.tasks.sync.TaskIndexing;
import com.algolia.search.objects.tasks.sync.TaskSingleIndex;
import com.algolia.search.responses.SearchResult;
import com.anshul.swiggy.eighttracks.web.dao.PlayList;
import com.anshul.swiggy.eighttracks.web.dao.Tag;
import com.anshul.swiggy.eighttracks.web.dao.User;
import com.anshul.swiggy.eighttracks.web.service.MainService;

@Controller
public class MainController {

	private MainService mainService;

	private static String ALGOLIA_APP_KEY = "R9VBPFCRAC";
	private static String ALGOLIA_API_KEY = "2acf7c3236a54d05cedf578ac9dbecc1";
	private APIClient ALGOLIA_CLIENT = new ApacheAPIClientBuilder(ALGOLIA_APP_KEY, ALGOLIA_API_KEY).build();
	
	private static int PLAY_WEIGHTAGE = 60;
	private static int FAVOURITE_WEIGHTAGE = 40;
	
	@Autowired
	public void setMainService(MainService mainService) {
		this.mainService = mainService;
	}
	
	/*---- CRUD APIs ---*/
	@RequestMapping(value="/tagexist", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> existTag(@RequestParam("name") String tagname) {
		
		Index<Tag> index = ALGOLIA_CLIENT.initIndex("tags", Tag.class);
		Map<String, Object> data = new HashMap<String, Object>();
		
		try {
			SearchResult<Tag> search = index.search(new Query(tagname).
			     setAttributesToRetrieve(Arrays.asList("id", "name", "playlistcount")).setHitsPerPage(5));
			for(Tag t : search.getHits()){
				if(tagname.equals(t.getName())){
					data.put("Status", "Success");
					data.put("tag", tagname);
					data.put("exist","true");
					data.put("tagid", t.getId());
					return data;
				}
			}
			
			data.put("Status", "Success");
			data.put("exist","false");
		} catch (AlgoliaException e) {
			e.printStackTrace();
			data.put("Status", "Failed");
		} 
		
		return data;
	}
	
	@RequestMapping(value="/createtag", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> createTag(@RequestParam("name") String tagname) {
		
		Tag tag = mainService.createTag(tagname);
		
		Map<String, Object> data = new HashMap<String, Object>();
		
		if(tag!=null){
			indextag();
			data.put("id", tag.getId());
			data.put("name", tag.getName());
		}
		
		return data;
	}

	@RequestMapping(value="/addtagtoplaylist", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> addTagToPlaylist(@RequestParam("tagid") String tagid, 
			@RequestParam("playlistid") String playlistid) {
		
		Map<String, Object> data = new HashMap<String, Object>();
		
		if(mainService.addTagToPlaylist(tagid, playlistid)){
			indextag();
			data.put("Status","Success");
			data.put("message","Tag mapped to Playlist");
		}else{
			data.put("Status","Failed");
		}
		
		return data;
	}
	
	/* Retreive all the playlists for a given tagid. */
	@RequestMapping(value="/getplaylists", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> getPlayLists(@RequestParam("tagid") String tagid) {
		Map<String, Object> data = new HashMap<String, Object>();
		
		List<PlayList> playlists = mainService.getPlayListsByTag(tagid);
		
		if(playlists!=null){
			/* The favourite count data for each playlist should ideally be pulled from Cache. */
			for(PlayList p : playlists){
				p.setFavourite(mainService.getPlaylistFavs(String.valueOf(p.getId())).size());
			}
			
			data.put("Status", "Success");
			data.put("playlists", playlists);
		}else{
			data.put("Status","Failed");
		}
		
		return data;
	}
	
	/* Retreive a playlist details (including its associated tags)
	 * */
	@RequestMapping(value="/getplaylist", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> getPlayList(@RequestParam("id") String playlistid) {
		Map<String, Object> data = new HashMap<String, Object>();
		PlayList playlist = mainService.getPlaylist(playlistid);		
		
		if(playlist!=null){
			List<Tag> taglist = mainService.getTagsForPlaylist(playlistid);
			
			/* This is an expensive operation. Cache should be used here.*/
			for(Tag t : taglist){
				t.setPlaylistcount(mainService.getPlayListCountByTag(String.valueOf(t.getId())));
			}
			
			List<User> userlist = mainService.getPlaylistFavs(playlistid);
			
			playlist.setTaglist(taglist);
			playlist.setFavourite(userlist.size());
			
			data.put("Status", "Success");
			data.put("playlist", playlist);
		} else {
			data.put("Status","Failed");
		}
		
		return data;
	}
	
	/* Explore API : Retrieves a list of relevant tags for a given search string. 
	 * Relevance of Results: This sorting logic is taken care by Algolia. The search result is sorted in the order of associated playlist count. 
	 * If search yields no results, populate with most popular tags.  
	 * */
	@RequestMapping(value="/explore", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> searchTag(@RequestParam("search") String searchstring) {
		
		Map<String, Object> data = new HashMap<String, Object>();
		
		Index<Tag> index = ALGOLIA_CLIENT.initIndex("tags", Tag.class);
		
		try {
			SearchResult<Tag> search = index.search(new Query(searchstring).
			     setAttributesToRetrieve(Arrays.asList("id", "name", "playlistcount")).setHitsPerPage(5));
			
			data.put("Status", "Success");
			
			if(!search.getHits().isEmpty()){
				data.put("suggestedTags", search.getHits());
			}else{
				/* If search yield no results, populate with most popular tags */
				List<Tag> taglist = mainService.getPlayListCountByTags();				
				data.put("popularTags", taglist);
			}
			
		} catch (AlgoliaException e) {
			e.printStackTrace();
			data.put("Status", "Failed");
		} catch(Exception e){
			data.put("Status", "Failed");
		}
		
		return data;
	}
	
	@RequestMapping(value="/indexuser", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> indexuser() {
		
		Map<String, Object> data = new HashMap<String, Object>();
		List<User> users = mainService.getAllUsers();
		
		Index<User> index = ALGOLIA_CLIENT.initIndex("users", User.class);
		try {			
			index.clear();
			TaskSingleIndex task = index.addObjects(users);
		} catch (AlgoliaException e) {
			e.printStackTrace();
			data.put("Error","Algolia Exception"+e.getMessage());
		}
		
		return data;
	}
	
	@RequestMapping(value="/indexplaylist", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> indexplaylist() {
		
		Map<String, Object> data = new HashMap<String, Object>();
		List<PlayList> playlist = mainService.getPlaylistByCountAndFavs();
		
		/* logic for relevance of playlist for a given tag */
		for(PlayList p: playlist){
			int weightage = p.getPlaycount() * PLAY_WEIGHTAGE + p.getFavourite() * FAVOURITE_WEIGHTAGE;
			p.setWeightage(weightage);
		}
		
		Index<PlayList> index = ALGOLIA_CLIENT.initIndex("playlist", PlayList.class);
		
		try {			
			index.clear();
			TaskSingleIndex task = index.addObjects(playlist);
		} catch (AlgoliaException e) {
			e.printStackTrace();
			data.put("Error","Algolia Exception"+e.getMessage());
		}
		
		return data;
	}
	
	@RequestMapping(value="/indextag", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> indextag() {
		
		Map<String, Object> data = new HashMap<String, Object>();
		List<Tag> taglist = mainService.getPlayListCountByTags();
		
		Index<Tag> index = ALGOLIA_CLIENT.initIndex("tags", Tag.class);
		
		try {			
			index.clear();
			TaskSingleIndex task = index.addObjects(taglist);
		} catch (AlgoliaException e) {
			e.printStackTrace();
			data.put("Error","Algolia Exception"+e.getMessage());
		}
		
		return data;
	}
	
	@RequestMapping(value="/getuser", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> getUser(@RequestParam("id") String id) {
		
		List<User> users = mainService.getUser(id);
		
		Map<String, Object> data = new HashMap<String, Object>();
		
		data.put("user", users);
		
		return data;
	}

}
