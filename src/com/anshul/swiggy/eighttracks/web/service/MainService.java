package com.anshul.swiggy.eighttracks.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anshul.swiggy.eighttracks.web.dao.PlayList;
import com.anshul.swiggy.eighttracks.web.dao.PlaylistDAO;
import com.anshul.swiggy.eighttracks.web.dao.Tag;
import com.anshul.swiggy.eighttracks.web.dao.TagDAO;
import com.anshul.swiggy.eighttracks.web.dao.User;
import com.anshul.swiggy.eighttracks.web.dao.UserDAO;

@Service("mainService")
public class MainService {

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private PlaylistDAO playlistDAO;
	
	@Autowired
	private TagDAO tagDAO;
	
	/*
	@Autowired
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Autowired
	public void setPlaylistDAO(PlaylistDAO playlistDAO) {
		this.playlistDAO = playlistDAO;
	}

	@Autowired
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
	 */
	
	public List<User> getUser(String id) {
		return userDAO.getUser(id);
	}
	
	public List<User> getAllUsers() {
		return userDAO.getAllUsers();
	}
	
	public PlayList getPlaylist(String id) {
		return playlistDAO.getPlaylist(id);
	}
	
	public List<PlayList> getAllPlaylists() {
		return playlistDAO.getAllPlaylists();
	}
	
	public List<Tag> getTag(String id) {
		return tagDAO.getTag(id);
	}
	
	public List<Tag> getAllTags() {
		return tagDAO.getAllTags();
	}
	
	public Tag createTag(String name){
		return tagDAO.createTag(name);
	}

	public List<Tag> getPlayListCountByTags(){
		return tagDAO.getPlayListCountByTags();
	}
	
	public int getPlayListCountByTag(String tagid){
		return tagDAO.getPlayListCountByTag(tagid).get(0).getPlaylistcount();
	}
	
	public List<Tag> getTagsForPlaylist(String playlistid){
		return tagDAO.getTagsForPlaylist(playlistid);
	}
	
	public List<PlayList> getPlayListsByTag(String tagid){
		return playlistDAO.getPlayListsByTag(tagid);
	}

	public List<PlayList> getPlaylistByCountAndFavs(){
		return playlistDAO.getPlaylistByCountAndFavs();
	}
	
	public List<User> getPlaylistFavs(String playlistid){
		return userDAO.getPlaylistFavs(playlistid);
	}
	
	public boolean addTagToPlaylist(String tagid, String playlistid){
		return playlistDAO.addTagToPlaylist(tagid, playlistid);
	}
}
