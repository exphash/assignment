Eight Tracks Mini Version (API Only)

Schema Location (MySQL DB dump)		: ./docs/Schema.sql
Schema EER Diagram			: ./docs/Schema_EER.pdf
API Documentation			: ./docs/EightTracksMini_API_doc.pdf

External Service used : Algolia (hosted elastic search)
Note: The Algolia search is under 14 day Free Trial. It will stop working on Sept 7, 2016.

Tomcat Version		: 		7.0
Java Version		: 		1.7.0_80
Database			: 		5.7.13 MySQL Community Server

API Summary

1. /tagexist?name=samba 
2. /createtag?name=new_tag
3. /addtagtoplaylist?tagid=10&playlistid=7
4. /getplaylists?tagid=4
5. /getplaylist?id=1
6. /explore?search=tagname
7. /indextag